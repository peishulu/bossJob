package com.boss.bossframeworkemail.service.impl;


import com.boss.bossframeworkemail.mapper.PhoneDaoMapper;
import com.boss.bossframeworkemail.pojo.UserLogin;
import com.boss.bossframeworkemail.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneControllerImpl implements PhoneService {

    @Autowired
    private PhoneDaoMapper phoneDao;


    @Override
    public int addPhone(UserLogin login) {
       if (login!=null) {
           phoneDao.addPhone(login);
           return 1;
       }
        return 0;
    }

    @Override
    public void add(UserLogin phone) {
        phoneDao.add(phone);
    }
}
