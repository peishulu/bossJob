package com.boss.bossframeworkweb.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 后台返回数据实体类
 */
@Data
public class Msg {
    //状态码
    private  Integer code;
    //消息
    private  String msg;
    //数据
    private Object data;

/*
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }*/
}
