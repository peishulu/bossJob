package com.boss.bossframeworkweb.config;

/**
 * 访问后台接口返回数据类
 */
public class ResponseUtil {

/**
 * 成功
 */
public static Msg success(Object object){

        Msg msg=new Msg();
        msg.setCode(200);
        msg.setMsg("请求成功");
        msg.setData(object);

    return msg;
}

    public static Msg success(){
        Msg msg=new Msg();
        msg.setCode(200);
        msg.setMsg("请求成功");
        return msg;
    }
    public static Msg success(Integer code,String msgs){
        Msg msg=new Msg();
        msg.setCode(code);
        msg.setMsg(msgs);
        return msg;
    }
    /**
     * 失败
     */
    public static Msg fail(Integer code,String m){
            Msg msg=new Msg();
            msg.setCode(code);
            msg.setMsg(m);
            return msg;
        }
        public static Msg fail(){
            Msg msg=new Msg();

            return msg;
        }
    }
