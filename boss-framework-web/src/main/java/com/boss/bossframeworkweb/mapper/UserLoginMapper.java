package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.UserLogin;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 登录简介 Mapper 接口
 * </p>
 *
 */
@Mapper
public interface UserLoginMapper extends BaseMapper<UserLogin> {

    /**
     * 登录方法
     */
    UserLogin findUserVoByUsernameAndPassword(UserLogin user);
    /**
     * 注册
     */
    int addPhone(UserLogin login);
}
