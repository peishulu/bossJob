package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.JobSearch;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobSearchMapper extends BaseMapper<JobSearch> {
    /**
     * 查询订阅职位总人数
     */
    int FindJobSearch();
}
