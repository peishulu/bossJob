package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.JobBasic;

public interface JobBasicMapper extends BaseMapper<JobBasic> {

}
