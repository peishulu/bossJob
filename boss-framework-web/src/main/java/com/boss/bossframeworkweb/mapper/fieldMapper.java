package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.field;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface fieldMapper extends BaseMapper<field> {
    List<field>findAllField();

}
