package com.boss.bossframeworkweb.service.impl;

import com.boss.bossframeworkweb.mapper.provinceMapper;
import com.boss.bossframeworkweb.pojo.province;
import com.boss.bossframeworkweb.service.provinceService;
import org.apache.ibatis.annotations.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class provinceServiceimpl implements provinceService {
    @Resource
    provinceMapper provinceMapper;
    @Override
    public List<province> getAll() {
        return provinceMapper.getAll();
    }
}
