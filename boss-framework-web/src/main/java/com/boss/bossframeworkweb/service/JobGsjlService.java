package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.JobGsjl;

public interface JobGsjlService {
    /**
     * 发布招聘职位
     */
    int AddJobGsjl(JobGsjl gsjl);
}
