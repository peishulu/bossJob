package com.boss.bossframeworkweb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.bossframeworkweb.pojo.company;
import com.boss.bossframeworkweb.pojo.development;

import java.util.List;

public interface developmentService extends IService<development> {

    List<development> findAllDevelopment();
}
