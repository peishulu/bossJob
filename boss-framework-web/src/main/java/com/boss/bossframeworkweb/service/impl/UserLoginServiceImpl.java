package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.UserLoginMapper;
import com.boss.bossframeworkweb.pojo.UserLogin;
import com.boss.bossframeworkweb.service.IUserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLogin> implements IUserLoginService {



    @Autowired
    private UserLoginMapper userDao;
    /**
     * 登录方法
     */
    @Override
    public UserLogin findUserVoByUsernameAndPassword(UserLogin user) {
        UserLogin userVoByUsernameAndPassword = userDao.findUserVoByUsernameAndPassword(user);
        return userVoByUsernameAndPassword;
    }
    /**
     * 注册
     */
    @Override
    public int addPhone(UserLogin login) {
        if (login!=null) {
            int i = userDao.addPhone(login);
            return i;
        }
        return 0;
    }

}
