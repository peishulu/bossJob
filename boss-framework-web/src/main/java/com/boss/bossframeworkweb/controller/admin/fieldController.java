package com.boss.bossframeworkweb.controller.admin;

import com.alibaba.fastjson.JSON;
import com.boss.bossframeworkweb.pojo.field;
import com.boss.bossframeworkweb.service.fieldService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Controller
public class fieldController {
    @Resource
    fieldService fieldService;

    @RequestMapping("/findAllField")
    @ResponseBody
    public String findAllField(){

        List<field> list=fieldService.findAllField();
        String json = JSON.toJSONString(list);
        return json;
    }
}
