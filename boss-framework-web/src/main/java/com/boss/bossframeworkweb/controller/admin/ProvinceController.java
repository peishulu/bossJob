package com.boss.bossframeworkweb.controller.admin;


import com.boss.bossframeworkweb.pojo.province;
import com.boss.bossframeworkweb.service.provinceService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ProvinceController {
    @Resource
    provinceService provinceService;

    @RequestMapping("/getAll")
    public String getAll(HttpServletRequest request){
        List<province>list=provinceService.getAll();
        request.setAttribute("province",list);
         return "companylist";
    }
}
