package com.boss.bossframeworkweb.controller.admin;

import com.boss.bossframeworkweb.pojo.JobBasic;
import com.boss.bossframeworkweb.pojo.JobGsjl;
import com.boss.bossframeworkweb.pojo.WorkTitle;
import com.boss.bossframeworkweb.service.JobBasicService;
import com.boss.bossframeworkweb.service.WorkTitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 我的简历模块
 */
@Controller
public class JobBasicController {

    @Autowired
    private JobBasicService jobBasicService;

    @Autowired
    private WorkTitleService workTitleService;

    /**
     * 基本信息新增
     * @return
     */
    @PostMapping("/addjobBasic")
    public String addjobBasic(JobBasic jobBasic, HttpServletRequest request) {
        int i = jobBasicService.addJobBasic(jobBasic);
        if (i!= 0) {
            System.out.println("测试:基本信息新增成功!");
            return "/companylist";
        }
        request.setAttribute("errorMsg","新增失败！");
        return "/jianli";
    }

    /**
     * 期望工作新增
     * @return
     */
    @PostMapping("/addWorkTitle")
    public String addWorkTitle(WorkTitle workTitle, HttpServletRequest request) {
       String work_state = request.getParameter("work_state");
       String work_name = request.getParameter("work_name");
       String work_salaried = request.getParameter("work_salaried");
        workTitle.setWorkState(work_state);
        workTitle.setWorkName(work_name);
        workTitle.setWorkSalaried(work_salaried);
        int i = workTitleService.addWorkTitle(workTitle);
        if (i!= 0) {
            System.out.println("测试:期望工作新增成功!");
            return "/companylist";
        }
        request.setAttribute("errorMsg","新增失败！");
        return "/jianli";
    }



}
