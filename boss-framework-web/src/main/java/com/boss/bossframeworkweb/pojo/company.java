package com.boss.bossframeworkweb.pojo;

import lombok.Data;

import java.util.List;

@Data
public class company {
    private  int companyId;
    private  String companyName;
    private  String companydevelopment;
    private String companyfield;
    private  String companyRecruitmentType;
    private  String companyMinimumeducation;
    private  String companySalaryRange;
    private  String companyJobDescription;
    private  String companyWorkplace;
    private  String companyImage;


    private List<development>developments;
    private  List<workplace>workplaces;
    private  List<education>educations;
    private List<field>fields;
}
