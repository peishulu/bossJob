package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

/**
 * JOB_BASIC
 * 
 * @author bianj
 * @version 1.0.0 2020-06-16
 */
public class JobBasic implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 5037039213478802918L;

    /** basicId */
    private Integer basicId;

    /** 姓名 */
    private String name;

    /** major */
    private String major;

    /** phone */
    private String phone;

    /** email */
    private String email;

    /** state */
    private String state;

    /** sex */
    private String sex;

    /** agelimit */
    private String agelimit;

    /**
     * 获取basicId
     * 
     * @return basicId
     */
    public Integer getBasicId() {
        return this.basicId;
    }

    /**
     * 设置basicId
     * 
     * @param basicId
     */
    public void setBasicId(Integer basicId) {
        this.basicId = basicId;
    }

    /**
     * 获取姓名
     * 
     * @return 姓名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置姓名
     * 
     * @param name
     *          姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取major
     * 
     * @return major
     */
    public String getMajor() {
        return this.major;
    }

    /**
     * 设置major
     * 
     * @param major
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * 获取phone
     * 
     * @return phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * 设置phone
     * 
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取email
     * 
     * @return email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * 设置email
     * 
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取state
     * 
     * @return state
     */
    public String getState() {
        return this.state;
    }

    /**
     * 设置state
     * 
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 获取sex
     * 
     * @return sex
     */
    public String getSex() {
        return this.sex;
    }

    /**
     * 设置sex
     * 
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取agelimit
     * 
     * @return agelimit
     */
    public String getAgelimit() {
        return this.agelimit;
    }

    /**
     * 设置agelimit
     * 
     * @param agelimit
     */
    public void setAgelimit(String agelimit) {
        this.agelimit = agelimit;
    }
}