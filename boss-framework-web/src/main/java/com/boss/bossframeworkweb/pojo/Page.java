package com.boss.bossframeworkweb.pojo;

        import lombok.Data;

        import java.util.List;

@Data
public class Page {
    //总页数
    private int totalPageCount;
    //页面大小，即每页显示记录数
    private int pageSize=3;
    //从第几条查起
    private int page;
    //记录总数
    private int totalCount;
    //当前页码
    private int currPageNo;
    //每页新闻集合
    private List<company> newsList;

    private int fId;

    private int dId;

    public int getfId() {
        return fId;
    }

    public void setfId(int fId) {
        this.fId = fId;
    }

    public int getdId() {
        return dId;
    }

    public void setdId(int dId) {
        this.dId = dId;
    }

    public int getPage(){
        return page;
    }

    public void setPage(int page){
        this.page = page;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if(pageSize>0)
            this.pageSize = pageSize;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        if(totalCount>0){
            this.totalCount = totalCount;
            totalPageCount=this.totalCount%pageSize==0?
                    (this.totalCount/pageSize):(this.totalCount/pageSize+1);
        }
    }

    public int getCurrPageNo() {
        if(totalPageCount==0)
            return 0;
        return currPageNo;
    }

    public void setCurrPageNo(int currPageNo) {
        if(currPageNo>0){
            this.currPageNo = currPageNo;
        }
    }

    public List<company> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<company> newsList) {
        this.newsList = newsList;
    }
}

