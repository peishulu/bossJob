package com.boss.bossframeworkweb.pojo;

import lombok.Data;

import java.util.List;

@Data
public class city {
    private  int id;
    private  int code;
    private  String name;
    private  int provincecode;
    private List<area>ares;
}
