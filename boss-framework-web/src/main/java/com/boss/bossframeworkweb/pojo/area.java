package com.boss.bossframeworkweb.pojo;

import lombok.Data;

@Data
public class area {
    private  int id;
    private  int code;
    private  String name;
    private  int citycode;
}
