package com.boss.bossframeworkweb.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;
/**
 * 主页列表1
 */
@Data
@TableName("a")
public class job1 {
    @Id
    private  Integer aid;
    private  String aname;
    private List<job2> getb;

}
